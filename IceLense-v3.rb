#!/usr/bin/env ruby
require 'slop'
require 'fileutils'
require 'open-uri'
require 'oj'
require 'tty-prompt'

# rubocop:disable Metrics/BlockLength, Metrics/LineLength, Style/AndOr, Style/IfInsideElse
# rubocop:disable Metrics/PerceivedComplexity, Metrics/MethodLength
# rubocop:disable Metrics/CyclomaticComplexity, Metrics/AbcSize, Style/ConditionalAssignment


@ice3 = nil
@t = nil
@sourcecount = nil
@sourceclass = nil
@listenarray = {}
@i = 0

@prompt = TTY::Prompt.new

helptext = "Welcome to IceLense v1.3
  -u, --url = specify the url
  -p, --port = specify the port
  -h, --help = display this text
  -v, --verbose = display extra output when script is running"


opts = Slop.parse do |o|
  o.on '-h', '--help', 'help' do

    puts helptext.to_s

    exit
  end
  o.string '-u', '--url', 'a url'
  o.string '-p', '--port', 'port (default: 8000)', default: 8000
  o.bool '-v', '--verbose', "'verbose output"
end

@url = opts[:url]
@port = opts[:port]
@verb = opts[:verbose]


def listenloop
  loop do
    listenurl = @sourceclass[@i].fetch('listenurl').split('/').at(-1)
    @listenarray.merge!("#{listenurl}": @i)
    unless @i == @sourcecount - 1
      @i += 1
    else
      break
    end
  end
end

def run(statusfile)
  unless @verb
    system('clear')
    system('cls')
  end
  iceopen = Oj.load(URI.open(statusfile))
  @sourcecount = iceopen.fetch('icestats').fetch('source').count
  @sourceclass = iceopen.fetch('icestats').fetch('source')
  if @sourceclass.is_a?(Array)
    listenloop
    sourcepick = @prompt.enum_select('Which mount point would you like?', @listenarray)
    sourcepick = sourcepick.to_i
  end
  unless @verb
    system('clear')
    system('cls')
  end
  loop do
    iceopen = Oj.load(URI.open(statusfile))
    ice1 = iceopen.fetch('icestats').fetch('source') # from here
    if ice1.is_a?(Array)
      if ice1.include? 'yp_currently_playing'
        ice1 = ice1.at(sourcepick).fetch('yp_currently_playing')
      else
        ice1 = ice1.at(sourcepick).fetch('title')
      end
    else
      if ice1.include? 'yp_currently_playing'
        ice1 = ice1.fetch('yp_currently_playing')
      else
        ice1 = ice1.fetch('title')
      end
    end
    if ice1 == @ice3
      sleep 1
    else
      if @verb
        puts iceopen
        puts 'Song change detected, updating output.'
      end
      song = File.open('songname.txt', 'w+')
      title = "► #{ice1}"

      song.each do |line|
        song.seek(-line.length, IO::SEEK_CUR)
        song.write(' ' * (line.length + 10))
        song.write("\r")
      end
      song.write("\r #{title} \r ")
      print "\033[K  #{title} \r"

      STDOUT.flush
      song.close
      sleep 1
      @ice3 = ice1
    end
  end
  gets
  @t.kill
end

if opts[:url].include? 'http://' or opts[:url].include? 'https://'
  source = "#{@url}:#{@port}/status-json.xsl"
else
  source = "http://#{@url}:#{@port}/status-json.xsl"
end
t1 = Thread.new { run(source) }
t1.join

# rubocop:enable Metrics/BlockLength, Style/ConditionalAssignment, Metrics/LineLength, Style/AndOr, Style/IfInsideElse
# rubocop:enable Metrics/PerceivedComplexity, Metrics/MethodLength
# rubocop:enable Metrics/CyclomaticComplexity, Metrics/AbcSize
