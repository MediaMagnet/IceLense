# IceLense

[![Dependency Status](https://beta.gemnasium.com/badges/gitlab.com/MediaMagnet/IceLense.svg)](https://beta.gemnasium.com/projects/gitlab.com/MediaMagnet/IceLense) ![license](https://img.shields.io/github/license/mashape/apistatus.svg)

IceLense is a tool that reads IceCast status-json.xsl files and provide the song title and artist

---

## Requirements 
- Ruby 2.4.3 or newer
- Oj v2.5.1 or newer
- Highline v1.7.10 or newer
- tty-prompt v0.16.0 or newer

## Set-up
1. clone or download this repository
2. `bundle install`

## Running
1. `./IceLense.rb -u ice.example.com:8001` no need to put in the xsl file as the script will handle that
2. If the server has more than one source you will be asked for the source number they are zero indexed
3. if you want to use the text file add it as a text source to obs it is called songname.txt
