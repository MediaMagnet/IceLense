#!/usr/bin/env ruby
require 'fileutils'
require 'open-uri'
require 'oj'
require 'tty-prompt'


helptext = "Welcome to IceLense enter a url using --url or -u or type --help\\-h\\-? to display this text \n"

$prompt = TTY::Prompt.new

para1 = ARGV[0].to_s.downcase.gsub(/\s+/, '')
if ARGV[1] != nil
	para2 = ARGV[1].downcase
end
$ice3 = nil

$t = nil
$sourcecount = nil
$sourceclass = nil
$listenarray = Hash.new
$i = 0

def listenloop
	loop do
		listenurl = $sourceclass[$i].fetch('listenurl').split('/').at(-1)
		$listenarray.merge!("#{listenurl}": $i)

		unless $i == $sourcecount - 1
			$i += 1
		else
			break
		end
	end
end

#Lets get the xsl file from the ice cast server
def run(statusfile)
	system('clear')
	system('cls')
	iceopen = Oj.load(open(statusfile))
	$sourcecount = iceopen.fetch('icestats').fetch('source').count
	$sourceclass = iceopen.fetch('icestats').fetch('source')
	if $sourceclass.is_a?(Array)
		listenloop
		sourcepick = $prompt.enum_select("Which mount point would you like?", $listenarray)

		sourcepick = sourcepick.to_i
	end
	system('clear')
	system('cls')
	loop do
		iceopen = Oj.load(open(statusfile))
		ice1 = iceopen.fetch('icestats').fetch('source')# from here
		if ice1.kind_of?(Array)
			ice1 = ice1.at(sourcepick).fetch('title')
		else
			ice1 = ice1.fetch('title')
		end

		if ice1 == $ice3
			sleep 1
		else
			song = File.open("songname.txt", 'w+')
			title = "► #{ice1}"

			song.each do |line|
				song.seek(-line.length, IO::SEEK_CUR)
				song.write(' ' * (line.length + 10))
				song.write("\r")
			end
			song.write("\r #{title} \r ")
			print "\033[K  #{title} \r"

			STDOUT.flush
			song.close
			sleep 1
			$ice3 = ice1
		end
	end
	gets
	$t.kill
end


if para1 == "--url" or para1 == "-u"
	unless para2
		print helptext
	else
		if para2.include? "http://" or para2.include? "https://"
			para2 = "#{para2}/status-json.xsl"
			t1 = Thread.new{ run(para2) }

			t1.join
		else
			para2 = "http://#{para2}/status-json.xsl"
			t1 = Thread.new{ run(para2) }

			t1.join
		end
	end
elsif para1 == "--help" or para1 == "-h" or para1 == "-?" or para1 == nil.to_s
	print helptext
end
