#!/usr/bin/env ruby
require 'eventmachine'
require 'video_info'
require 'websocket-eventmachine-client'
require 'configatron'
require_relative 'defaults.rb'

EM.run do
	ws = WebSocket::EventMachine::Client.connect(:host => 'irc-ws.chat.twitch.tv', :port => 80, :ssl => false)

	ws.onopen do
		puts "Connected"
		ws.send "CAP REQ :twitch.tv/tags twitch.tv/commands twitch.tv/membership"
		ws.send "PASS #{configatron.twitch.oauth}"
		ws.send "NICK #{configatron.irc.nick}"
		ws.send "JOIN #{configatron.twitch.irc}"

		ws.send "PRIVMSG #{configatron.twitch.irc} :#{configatron.welcome.message}"

		#ws.send "PRIVMSG #mediamagnet :/me whale hello there, Musicbot v2 connected."

	end
	ws.onmessage do |msg, type|
		if msg.include?('PING') == true
			ws.send "PONG :tmi.twitch.tv"

		elsif msg.include?(' PRIVMSG ')
			#metadata = msg.split(' ')[0]
			user_msg_arr = msg.split(' ')
			user_msg_arr.shift
			user_msg_arr.shift
			user_msg_arr.shift
			user_msg_arr.shift
			user_msg_arr[0] = user_msg_arr[0].delete_prefix(':')


			if user_msg_arr[0] == '!playing'
				File.open("songname.txt", "r").each do |line|
					ws.send "PRIVMSG #{configatron.twitch.irc} :Everybody stop look what's that sound? #{line}"

				end

			end


		elsif msg.include?(' JOIN ') || msg.include?(' PART ')

		else
			puts msg
		end
	end

	ws.onclose do |code, reason|
		puts "Disconnected with status code: #{code} #{reason}"

	end

	ws.onerror do |error|
		puts "Error: #{error}"

	end

	EventMachine.next_tick do
		puts "tick!"
	end
end
